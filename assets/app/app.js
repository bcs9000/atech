//declara app travelApp
var travelApp = angular.module('travelApp',[]);
//cria controller
travelApp.controller('travelController', function($scope,$http){
//função para ser ativada somente no submit do form
$scope.submit = function() {
    //puxa conteúdo do json
    $http.get("../data.json")
        //se rolar...
        .then(function(response) {
            //instancia resultados do array items do json como $scope.names
            $scope.names = response.data.items;
        });
    };  
});